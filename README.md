For the original documentation for the Dockerised Pact Broker, please refer to https://github.com/pact-foundation/pact-broker-docker/blob/master/README.md.

# Setting Up Pact Broker Instance with Google Cloud Engine
## Configuring Google Cloud Services

Before we boot up the Pact Broker instance in Google Cloud Engine, there are a few services that need to be set up first. In this guide, we will be setting this up using the Google Cloud Platform UI.

### Authenticating Docker using Service Account Key

1. In the Google Cloud Platform UI and under IAM & Admin, go to Service Accounts and click on the `Create Service Account` button.
2. Fill in the `Service account Name` and the `Service account ID` field then click `Done`.
3. Back in the Service Accounts list page, click on the Actions ellipsis for the newly created Service Account and select `Manage Keys`.
4. Click on the `Add Keys` button and select `Create new key` in JSON format. With this key, we can use this Service Account to authenticate Docker.
5. Clone this repository and navigate to the directory in the console.
6. In the terminal, run the following:
    ```sh
      key=SERVICE_ACCOUNT_KEY_FILE_PATH
      cat "$key" | docker login -u _json_key --password-stdin https://gcr.io
    ```

### Creating the Compute Engine Instance

1. In the Google Cloud Platform UI, go to Compute Engine and click `Create Instance`.
2. Name the instance and select the Service Account that was created earlier.
3. Tick the box for `Allow HTTP traffic` and `Allow HTTPS traffic`.
4. Click on  `Create` which will start spinning up the instance.

### Setting Up the Compute Engine Instance

This step assumes that the `gcloud` CLI has been installed.

<!-- 1. Activate the Service Account for the `gcloud` CLI by running `gcloud auth activate-service-account --key=SERVICE_ACCOUNT_KEY_FILE_PATH` -->
2. In the Google Cloud Platform UI, go to the Compute Engine instance that was just created and click on the `SSH` dropdown and select `View gcloud command`.
3. Copy the command and run that on your terminal.
4. Install Docker by following the steps documented in https://docs.docker.com/engine/install/debian/.
5. Verify your installation by running `docker ps`. If that fails due to a permissioning error, run `sudo -i` to switch to the root user.
6. Run `gcloud auth configure-docker --quiet` to configure Docker.
7. Install `git` in the instance (https://www.digitalocean.com/community/tutorials/how-to-install-git-on-debian-10).
8. Set up `git` in order to clone this repository.
9. Clone this repository to the instance.
10. Edit the `docker-compose.yml` in the terminal and update [`PACT_BROKER_BASE_URL`](./docker-compose.yml#L33) to use the Internal IP listed for the Compute Engine Instance.
    * You will notice that some url are already defined there. Simply update the IP for those.
11. Edit the [`nginx.conf`](./ssl/nginx.conf) and update the 2 areas where `server_name` is defined and add the same Internal IP.
12. Go into the cloned repository's directory and run `docker compose up`
    * You might notice that `certbot` didn't boot up. That's okay at this stage. The repository includes a base set of modification from the original `pact-broker-docker` repo that would still allow it to boot up the instance without an SSL certification set up.

The Pact Broker instance should be up now but the browser will likely display a warning as there is no SSL certificate yet.

### Generating and Setting Up SSL Certificate

1. Run `docker compose run --rm  certbot certonly --webroot --webroot-path /var/www/certbot --dry-run -d 34.171.114.134.nip.io` and ensure the dry run is successful.
2. Run `docker compose run --rm  certbot certonly --webroot --webroot-path /var/www/certbot -d 34.171.114.134.nip.io` to actually create the certificate now.
    * You can verify this by going to `/etc/letsencrypt`
3. Configure `nginx` to point to the generated certificates by uncommenting `ssl_certificate` and `ssl_certificate_key` and commenting out the previous two others.
4. Restart again with `docker compose restart`

The instance should now have a valid SSL certificate when going to the `.nip.io` url.

<!-- ### Configuring Service Account Roles for SSH Access

1. In the Google Cloud Platform UI and under IAM & Admin, go to IAM.
2. Edit the same entry that was used to setup the `Storage Admin` role earlier.
3. Add the `Compute Admin` role.
4. Add the `Service Account User` role.
5. Add the `IAP-secured Tunnel User` role. -->

